FROM python:2.7.15-alpine

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

COPY watchdog.py /usr/bin/watchdog

RUN chmod +x /usr/bin/watchdog

ENTRYPOINT /usr/bin/watchdog
