#!/usr/local/bin/python

import sdnotify
import time
import math
import os
#from multiprocessing import Pool
import urllib2


class WatchdogMonitor:

    def __init__(self,report_interval,tolerance=3):
        self.report_interval = report_interval * math.pow(10,-6)
        self.notifier = sdnotify.SystemdNotifier()
        self.tolerance = tolerance

    def report(self):
        self.notifier.notify("WATCHDOG=1")

    def report_loop(self,condition):
       cont = self.tolerance
       while cont > 0:
           try:
               self.report()
           except:
               print("Failed to report to systemd")
           cont_start = time.time()
           cont = self.tolerance if condition() else cont - 1
           if cont != self.tolerance and cont > 0:
               print("Failed condition %s attempts left" % str(cont))
           time.sleep(max((self.report_interval * 0.75) - (time.time() - cont_start),1))

class PingService:

    def __init__(self, urls, timeout=15):
       self.urls = urls
       self.timeout = int(timeout)
       #self.pool = Pool()

    def ping(self, url, expec):
       code = 0
       starttime = time.time()
       try:
           print "Ping: %s" % url
           resp = urllib2.urlopen(url, timeout = self.timeout)
           code = str(resp.getcode())
       except urllib2.HTTPError as e:
           code = str(e.code)
       except:
           code = 0
       if code > 0:
           print("Pinged %s -> %s, %s" % (url,str(code),"ok, took: %sms" % str((time.time() - starttime)*1000) if code == expec else "expected %s" % expec))
       else:
           print("Ping failed: %s, service not available at all" % url)
       return code == expec

    def ping_all(self):
        return list(map(lambda up: self.ping(*up),urls))
        #return self.pool.map(self.ping, self.urls)

if __name__ == "__main__":

    def envor(name,alt):
       if name in os.environ:
          return os.environ[name]
       return alt

    freq = int(envor("URLWATCHDOG_FREQ",1))
    tolerance = int(envor("URLWATCHDOG_TOCE",3))
    interval = float(envor('WATCHDOG_USEC',math.pow(10,7))) / freq
    urls = list(map(lambda pair: tuple(pair.split("=")),str(envor('URLWATCHDOG','')).split(',')))
    if ('',) in urls: urls.remove(('',))
    if len(urls) == 0:
        print("No urls specified")
    if interval and len(urls) > 0:
        print("Started Urlwatchdog, interval: %ss, tolerance: %s" % (str(int(interval * math.pow(10,-6))),str(tolerance)))
        mon = WatchdogMonitor(float(interval),tolerance)
        pinger = PingService(urls, interval*0.75)
        def ping():
            for res in pinger.ping_all():
                if not res:
                    return False
            return True
        mon.report_loop(ping)
